#!/bin/bash
# Maintainer: Nico Meisenzahl <nico@meisenzahl.org>
# Not intended for production!

# Customize based on your environment

# Disable colors
export BLUEMIX_COLOR=false

# Delete the cluster
yes | ibmcloud ks cluster-rm soccnx_cluster
